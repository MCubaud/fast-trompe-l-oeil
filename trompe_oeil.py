import numpy as np
import matplotlib.pyplot as plt
from skimage import io as skio

plt.close('all')

# A4 sheet 300 dpi
width=2480
height=3508

#Picture to open
file = 'cube-bot.jpg'
img = plt.imread(file)

Corners=["Bottom Left", "Bottom Right", "Top Right", "Top Left"]
projected=0

# Getting the coordinates of the corners
fig= plt.figure('picture')
ax = plt.imshow(img)

x_sheet_corners=[]#Coordinates in the picture referentiel
y_sheet_corners=[]
count=0
fig.suptitle("Pick the "+Corners[count]+" corner of the paper sheet on the image")

def onclick(event):
    """
    Add the clicked point to the list of corners. Then, when 4 corners have been selected, launch the reprojection of the paper sheet
    """
    global x_sheet_corners,y_sheet_corners,count,fig,ax
    #print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
          # ('double' if event.dblclick else 'single', event.button,
          #  event.x, event.y, event.xdata, event.ydata))
    if event.dblclick:
        x_sheet_corners.append(event.xdata)
        y_sheet_corners.append(event.ydata)
        count+=1
        if count>=4:
            fig.canvas.mpl_disconnect(cid)
            fig.suptitle("")
            plt.plot(x_sheet_corners+[x_sheet_corners[0]],y_sheet_corners+[y_sheet_corners[0]],color='b')
            plt.plot(x_sheet_corners,y_sheet_corners,'o',color='r')
            fig.canvas.draw_idle()
            suite()
        else:
            fig.suptitle("Pick the "+Corners[count]+" corner of the paper sheet on the image")
            plt.plot(x_sheet_corners,y_sheet_corners,'o',color='r')
            fig.canvas.draw_idle()

def suite():
    """
    Reprojection of the paper sheet
    """
    global x_sheet_corners,y_sheet_corners,projected
    print("Determination of the transformation")
    #print(x_sheet_corners,y_sheet_corners)
    a1,b1,c1,a2,b2,c2,a3,b3=determinate_transformation_8()
    # a1,b1,c1,a2,b2,c2=determinate_transformation()
    print("Projection of the image on the sheet")
    projected=project_image_on_sheet_8(a1,b1,c1,a2,b2,c2,a3,b3)
    # projected=project_image_on_sheet(a1,b1,c1,a2,b2,c2)
    plt.figure('Projected image')
    plt.imshow(projected/255)
    skio.imsave('projection.png',projected)
    plt.show()

cid = fig.canvas.mpl_connect('button_press_event', onclick)


plt.show()

def determinate_transformation_8():
    """
    Determine the 8 parameters of the homotethy between the paper and its image using mean square
    """
    global x_sheet_corners,y_sheet_corners

    i_corners=np.array([[1,height,height,1]]).T
    j_corners=np.array([[1,1,width,width]]).T
    ones=np.ones((4,1))
    a1,b1,c1,a2,b2,c2,a3,b3=[1,0,min(x_sheet_corners),0,1,min(y_sheet_corners),0,0]

    Norme_B=np.Infinity
    i=0
    while Norme_B>1e-3 and i<100:
        i+=1

        numerator_1=a1*i_corners+b1*j_corners+c1
        numerator_2=a2*i_corners+b2*j_corners+c2
        denominator=a3*i_corners+b3*j_corners+1
        f_x0=np.vstack((numerator_1/denominator,numerator_2/denominator))
        B=np.vstack((np.array([x_sheet_corners]).T,np.array([y_sheet_corners]).T))-f_x0
        Norme_B=np.linalg.norm(B)
        #print("Norme de B :" + str(Norme_B))

        #Construction of the model matrix
        A_0=np.hstack((i_corners/denominator,j_corners/denominator,ones/denominator))
        A_1=np.hstack((-i_corners*numerator_1/(denominator**2),-j_corners*numerator_1/(denominator**2)))
        A_2=np.hstack((-i_corners*numerator_2/(denominator**2),-j_corners*numerator_2/(denominator**2)))
        A=np.vstack((np.hstack((A_0,np.zeros(A_0.shape))),np.hstack((np.zeros(A_0.shape),A_0))))
        A=np.hstack((A,np.vstack((A_1,A_2))))
        N=A.T.dot(A)
        K=A.T.dot(B)
        delta_X=np.linalg.solve(N,K)
        N_delta_X=np.linalg.norm(delta_X)
        a1,b1,c1,a2,b2,c2,a3,b3=(delta_X+np.array([[a1,b1,c1,a2,b2,c2,a3,b3]]).T).reshape((8,1))
        #print(str(i) + " itérations : N_delta_X = " + str(N_delta_X))
        #print(a1,b1,c1,a2,b2,c2,a3,b3)
    plt.plot(f_x0[:4],f_x0[4:],'o-',color='g')
    fig.canvas.draw_idle()
    return (a1,b1,c1,a2,b2,c2,a3,b3)



def project_image_on_sheet_8(a1,b1,c1,a2,b2,c2,a3,b3):
    """
    Use the 8 parameters estimated to project the picture on the paper
    """
    projected = np.zeros((height,width,3))
    les_i = np.repeat(np.arange(height).reshape((height,1)),width,axis=1)
    les_j = np.repeat(np.arange(width).reshape((width,1)),height,axis=1).T
    les_x = np.minimum(np.int64((a1*les_i+b1*les_j+c1)/(a3*les_i+b3*les_j+1)),img.shape[0]-1)
    les_y = np.minimum(np.int64((a2*les_i+b2*les_j+c2)/(a3*les_i+b3*les_j+1)),img.shape[1]-1)
    projected=img[les_y,les_x]
    return projected

