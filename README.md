# Fast Trompe l'Oeil


## Description
The objective of Fast Trompe l'Oeil is to create an anamorphose from a picture.
You take a picture of a scene on which there is a sheet of paper. You can put any object on this sheet as long as it does not overstep the sheet seen from the camera point of view.
Then, the algorithm projects the sheet seen from the camera to a plane sheet of paper, and so if you print it, you can create the illusion of the objects if the paper is placed at the same place and the observer is looking from the same direction as the camera.


## Example
Here is for instance a library, with on the table two sheets of paper and a small robot above.
![image-2.png](./image-2.png)
And here are the output of the algorithm applied on each sheet separately.
![projection_1.png](./projection_1.png)
![projection_2.png](./projection_2.png)
